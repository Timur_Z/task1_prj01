import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Random;


/* 1 задание:
•	Создать целочисленный массив, заполнить его случайными числами от 0 до 20.
•	Вывести в стандартный поток вывода.
•	Отсортировать в цикле for.
•	Вывести отсортированный массив.
*/
public class Prj01 {

    public static void main(String[] args) {
		System.out.println ("Создаем массив и заполняем его случайными числами от 0 до 20 и выводим на консоль.");
        int[] arr = new int[20];
        Random random = new Random();

        for (int i = 0; i < arr.length; i++) {
            arr[i] = random.nextInt(20);
            System.out.print(arr[i] + " ");
        }
		
        System.out.println("Сортируем и выводим на консоль");


        for (int i = arr.length - 1; i > 0; i--) {
            for (int j = 0; j < i; j++) {
                if (arr[j] > arr[j + 1]) {
                    int tmp = arr[j];
                    arr[j] = arr[j + 1];
                    arr[j + 1] = tmp;
                }
            }
        }

        System.out.println(Arrays.toString(arr));
    }
}
